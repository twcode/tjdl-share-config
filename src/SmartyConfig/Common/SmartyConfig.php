<?php

namespace SmartyConfig\Common;

class SmartyConfig
{
    public static function configDir()
    {
        $dirArray = array(
            S_ROOT . 'vendor/twcode/tjdl-share-config/src/SmartyConfig/Dl',
            S_ROOT . 'vendor/twcode/tjdl-share-config/src/SmartyConfig/Common',
            S_ROOT . 'vendor/twcode/tjdl-share-config/src/SmartyConfig/',
        );

        return $dirArray;
    }
}
